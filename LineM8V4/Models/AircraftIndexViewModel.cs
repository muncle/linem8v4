﻿using LineM8V4.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LineM8V4.Models
{
    public class AircraftIndexViewModel
    {
        public string AircraftMSN { get; set; }
        public IEnumerable<AircraftType> AircraftType { get; set; }
        public IEnumerable<EngineType> EngineType { get; set; }

        public IEnumerable<Airline> Airline { get; set; }
        public string AircraftRegistration { get; set; }

        public string SearchQuery { get; set; }
    }
}
