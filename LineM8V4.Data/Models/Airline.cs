﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V4.Data.Models
{
    public class Airline
    {
        public int AirlineId { get; set; }
        public string AirlineName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
