﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V4.Data.Models
{
    public class EngineType
    {
        public int EngineTypeId { get; set; }
        public string EngineTypeName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
