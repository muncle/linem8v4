﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V4.Data.Models
{
    public class AircraftType
    {
        public int AircraftTypeId { get; set; }
        public string AircraftTypeName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
