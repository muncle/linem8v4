﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V4.Data.Models
{
    public class Aircraft
    {
        public int AircraftId { get; set; }
        public string AircraftMSN { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}