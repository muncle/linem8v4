﻿using LineM8V4.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace LineM8V4.Data
{
    public class LineM8V4DbContext : DbContext
    {
        public LineM8V4DbContext(DbContextOptions options) : base(options)
        {

        }
        
        public DbSet<Aircraft> Aircraft { get; set; }
        public DbSet<AircraftType> AircraftTypes { get; set; }
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<EngineType> EngineTypes { get; set; }

    }
}
